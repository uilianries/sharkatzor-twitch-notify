import requests
import logging
import argparse
import configparser
from datetime import datetime


LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)
S_HANDLER = logging.StreamHandler()
S_HANDLER.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(message)s')
S_HANDLER.setFormatter(formatter)
LOGGER.addHandler(S_HANDLER)
RFC3339 = '%Y-%m-%dT%H:%M:%SZ'


class TwitchStream(object):
    def __init__(self, id="", title="", started_at="", is_alive=False):
        self.id = id
        self.title = title
        self.started_at = started_at or datetime.strftime(datetime(2000, 1, 1), RFC3339)
        self.is_alive = is_alive
        self._config = configparser.ConfigParser()
        self._config["stream"] = {"id": self.id, "title": self.title, "started_at": self.started_at}

    def __eq__(self, other):
        return self.id == other.id

    def __repr__(self):
        return f"{self.id}: {self.started_at}: {self.title} ({'live' if self.is_alive else 'offline'})"

    @staticmethod
    def link(name):
        return f"https://www.twitch.tv/{name}"

    def store(self, config_path):
        with open(config_path, 'w') as fd:
            self._config.write(fd)
        return True

    @staticmethod
    def restore(config_path):
        try:
            config = configparser.ConfigParser()
            config.read(config_path)
            return TwitchStream(config["stream"]["id"], config["stream"]["title"], config["stream"]["started_at"])
        except Exception:
            return TwitchStream()


class TwitchScrapper:

    def __init__(self, logger, client_id, client_secret):
        self._logger = logger
        self._client_id = client_id
        self._client_secret = client_secret
        self._access_token = None

    def _is_logged_in(self):
        if self._access_token:
            params = {'Authorization': "Bearer " + self._access_token, 'Client-ID': self._client_id}
            response = requests.get(url='https://id.twitch.tv/oauth2/validate', headers=params)
            if response.ok:
                return True
            else:
                self._logger.info(f"Twitch login expired: {response.text}")
        return False

    def get_stream(self, channel):
        if not self._is_logged_in():
            self._logger.debug("Twitch login expired")
            self._login_twitch()
        params = {'Client-ID': self._client_id, 'Authorization':  "Bearer " + self._access_token}
        response = requests.get(f'https://api.twitch.tv/helix/streams?user_login={channel}', headers=params)
        response.raise_for_status()
        data = response.json()['data']
        if data:
            is_alive = data[0]['type'] == 'live'
            started_at = data[0]['started_at'] if is_alive else ""
            self._logger.debug("Is alive on Twitch: {}".format("YES" if is_alive else "NO"))
            return TwitchStream(data[0]['id'], data[0]['title'], started_at, is_alive)

    def _login_twitch(self):
        self._logger.debug("Login on Twitch")
        params = {'client_id': self._client_id, 'client_secret': self._client_secret, 'grant_type': 'client_credentials'}
        response = requests.post(url='https://id.twitch.tv/oauth2/token', data=params)
        response.raise_for_status()
        if not response.ok:
            raise Exception(f"Could not login on Twitch: {response.text}")
        data = response.json()
        if "access_token" not in data:
            raise Exception(f"Could not find access_token: {response}")
        self._access_token = data["access_token"]
        self._logger.debug("Logged in on Twitch: {}*****".format(self._access_token[:4]))
        return True


class DiscordMessenger:

    def __init__(self, logger, access_token, public_channel, maintenance_channel):
        self._logger = logger
        self._public_channel = public_channel
        self._maintenance_channel = maintenance_channel
        self._access_token = access_token

    def _send_message(self, channel, message):
        response = requests.post(
            f"https://discordapp.com/api/channels/{channel}/messages",
            headers={"Authorization": f"Bot {self._access_token}"},
            json={"content": message}
        )
        response.raise_for_status()
        json_data = response.json()
        return json_data['id']

    def send_general_message(self, message):
        self._logger.debug("Sending message to general channel on Discord: {}".format(message))
        self._send_message(self._public_channel, message)

    def send_maintenance_message(self, message):
        self._logger.debug("Sending message to maintenance channel on Discord: {}".format(message))
        self._send_message(self._maintenance_channel, message)


class SharkatzorConfiguration:

    def __init__(self, logger, config_file_path):
        self._logger = logger
        self._config = self._read_config(config_file_path)

    def _read_config(self, config_file_path):
        config = configparser.ConfigParser()
        config.read(config_file_path)
        return config

    def _log_configuration(self):
        self._logger.info(f'Twitch channel: {self._config["twitch"]["channel"]}')
        self._logger.info("Twitch client ID: {}****".format(self._config["twitch"]["client_id"][:4]))
        self._logger.info("Twitch client secret: {}****".format(self._config["twitch"]["client_secret"][:4]))
        self._logger.info(f'Twitch cooldown hours: {self._config["twitch"]["cooldown_hours"]}')
        self._logger.info('Discord Token: {}****'.format(self._config["discord"]["token"][:4]))
        self._logger.info('General Discord channel: {}****'.format(self._config["discord"]["general_channel"][:4]))
        self._logger.info('Maintenance Discord channel: {}****'.format(self._config["discord"]["maintenance_channel"][:4]))

    @property
    def configuration(self):
        return self._config


class SharkatzorTwitchNotifier:

    def __init__(self, config_path):
        self._logger = LOGGER
        self._logger.info('Starting ...')
        self._config = self._load_configuration(config_path)

    def _load_configuration(self, config_path):
        shark_config = SharkatzorConfiguration(self._logger, config_path)
        return shark_config.configuration

    def _is_stale_stream(self, stream, last_stream):
        started_at = datetime.strptime(stream.started_at, RFC3339)
        last_started_at = datetime.strptime(last_stream.started_at, RFC3339)
        delta = started_at - last_started_at
        return delta.total_seconds() > int(self._config["twitch"]["cooldown_hours"]) * 60 * 60

    def run(self):
        twitch = TwitchScrapper(self._logger, self._config["twitch"]["client_id"], self._config["twitch"]["client_secret"])
        stream = twitch.get_stream(self._config["twitch"]["channel"])
        if stream:
            cache_stream = TwitchStream.restore(self._config["store"]["path"])
            if cache_stream != stream and self._is_stale_stream(stream, cache_stream):
                self._logger.info(f"Is alive on Twitch: ({stream.id}): '{stream.title}'")
                discord = DiscordMessenger(self._logger, self._config["discord"]["token"], self._config["discord"]["general_channel"], self._config["discord"]["maintenance_channel"])
                try:
                    link = TwitchStream.link(self._config["twitch"]["channel"])
                    discord.send_general_message(f"Tomahawk está ao vivo na Twitch @everyone!\n**{stream.title}**\n{link}")
                    stream.store(self._config["store"]["path"])
                except Exception as err:
                    self._logger.error(f"Could not send message to Discord: {err}")
                    discord.send_maintenance_message(f"Caramba, não consegui mandar mensagem no Discord!:\n{err}")


def main():
    argparser = argparse.ArgumentParser(description="Sharkatzor Twitch Notifier for Discord")
    argparser.add_argument("-c", "--config", help="Configuration file path", default="/home/uilian/Development/sharkatzor-twitch-notify/conf.ini")
    args = argparser.parse_args()

    sharkatzor = SharkatzorTwitchNotifier(args.config)
    sharkatzor.run()


if __name__ == "__main__":
    main()

