import pytest
import tempfile



@pytest.fixture
def mock_twitch_is_alive(requests_mock):
    requests_mock.get('http://test.com', text='data')
    return requests_mock

@pytest.fixture
def notifier_config():
    temp = tempfile.TemporaryFile()
    path = temp.name
    temp.write(b'[twitch]')
    temp.write(b'channel=foobar')
    temp.write(b'client_id=pj44po8f8cgs8sfhlw4abmgit26422')
    temp.write(b'client_secret=aaassscikpjqmtavqqgs3mtyj7mxtt')
    temp.write(b'cooldown_hours=12\n')
    temp.write(b'[store]')
    temp.write(b'path=test.store')
    temp.write(b'[discord]')
    temp.write(b'token=jc2Nzg1MT')
    temp.write(b'general_channel=12345670')
    temp.write(b'maintenance_channel=987523')
    temp.close()
    return path


def test_twitch_is_alive(notifier_config):
    pass