from sharkatzor.sharkatzor_twitch_notify import TwitchScrapper
from sharkatzor.sharkatzor_twitch_notify import LOGGER

import pytest
import requests_mock


def test_twitch_scrapper_regular():
    scrapper = TwitchScrapper(LOGGER, "client_id", "client_secret")
    assert scrapper._client_id == "client_id"
    assert scrapper._client_secret == "client_secret"
    assert scrapper._logger == LOGGER


def test_twitch_scrapper_is_logged():
    response = {"client_id": "wbmytr93xzw8zbg0p1izqyzzc5mbiz",
                "login": "twitchdev",
                "scopes": ["channel:read:subscriptions"],
                "user_id": "141981764",
                "expires_in": 5520838}
    with requests_mock.Mocker() as mocker:
        mocker.get('https://id.twitch.tv/oauth2/validate', json=response)
        scrapper = TwitchScrapper(LOGGER, "client_id", "client_secret")
        scrapper._access_token = 'access_token'
        assert scrapper._is_logged_in() == True


def test_twitch_scrapper_login_success():
    response = {
          "access_token": "0123456789abcdefghijABCDEFGHIJ",
          "refresh_token": "eyJfaWQmNzMtNGCJ9%6VFV5LNrZFUj8oU231/3Aj",
          "expires_in": 5215742,
          "scope": "channel:read:subscriptions",
          "token_type": "bearer"}
    with requests_mock.Mocker() as mocker:
        mocker.post('https://id.twitch.tv/oauth2/token', json=response)
        scrapper = TwitchScrapper(LOGGER, "client_id", "client_secret")
        assert scrapper._login_twitch() == True


def test_twitch_scrapper_login_failure():
    response = {
          "error": "Bad Request",
          "status": 400,
          "message": "Invalid refresh token"}
    with requests_mock.Mocker() as mocker:
        mocker.post('https://id.twitch.tv/oauth2/token', json=response, status_code=401)
        scrapper = TwitchScrapper(LOGGER, "client_id", "client_secret")
        try:
            scrapper._login_twitch()
            pytest.fail("Should have raised an exception")
        except Exception as error:
            assert "401 Client Error" in str(error)


def test_twitch_scrapper_get_stream_success():
    scrapper = TwitchScrapper(LOGGER, "client_id", "client_secret")
    with requests_mock.Mocker() as mocker:
        response = {
            "access_token": "0123456789abcdefghijABCDEFGHIJ",
            "refresh_token": "eyJfaWQmNzMtNGCJ9%6VFV5LNrZFUj8oU231/3Aj",
            "expires_in": 5215742,
            "scope": "channel:read:subscriptions",
            "token_type": "bearer"}
        mocker.post('https://id.twitch.tv/oauth2/token', json=response)
        response = {"client_id": "wbmytr93xzw8zbg0p1izqyzzc5mbiz",
                    "login": "twitchdev",
                    "scopes": ["channel:read:subscriptions"],
                    "user_id": "141981764",
                    "expires_in": 5520838}
        mocker.get('https://id.twitch.tv/oauth2/validate', json=response)
        response = {"data": [
            {
                  "id": "123456789",
                  "user_id": "98765",
                  "user_login": "sandysanderman",
                  "user_name": "SandySanderman",
                  "game_id": "494131",
                  "game_name": "Little Nightmares",
                  "type": "live",
                  "title": "hablamos y le damos a Little Nightmares 1",
                  "tags": ["Español"],
                  "viewer_count": 78365,
                  "started_at": "2021-03-10T15:04:21Z",
                  "language": "es",
                  "thumbnail_url": "https://static-cdn.jtvnw.net/previews-ttv/live_user_auronplay-{width}x{height}.jpg",
                  "tag_ids": [],
                  "is_mature": False
                },
              ],
              "pagination": {
                "cursor": "eyJiIjp7IkN1cnNvciI6ImV5SnpJam8zT0RNMk5TNDBORFF4TlRjMU1UY3hOU3dpWkNJNlptRnNjMlVzSW5RaU9uUnlkV1Y5In0sImEiOnsiQ3Vyc29yIjoiZXlKeklqb3hOVGs0TkM0MU56RXhNekExTVRZNU1ESXNJbVFpT21aaGJITmxMQ0owSWpwMGNuVmxmUT09In19"
              }
            }
        mocker.get('https://api.twitch.tv/helix/streams?user_login=foobar', json=response)

        twitch_stream = scrapper.get_stream("foobar")
        assert twitch_stream.id == "123456789"
        assert twitch_stream.title == "hablamos y le damos a Little Nightmares 1"
        assert twitch_stream.started_at == "2021-03-10T15:04:21Z"
        assert twitch_stream.is_alive == True

        response = {"data": []}
        mocker.get('https://api.twitch.tv/helix/streams?user_login=foobar', json=response)
        twitch_stream = scrapper.get_stream("foobar")
        assert twitch_stream is None
