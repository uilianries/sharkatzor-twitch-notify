from sharkatzor.sharkatzor_twitch_notify import TwitchStream

import os
import tempfile


def test_twitch_stream_regular():
    stream = TwitchStream("123", "Foobar Qux!", "2020-01-01T00:00:00Z", True)
    assert stream.id == "123"
    assert stream.started_at == "2020-01-01T00:00:00Z"
    assert stream.title == "Foobar Qux!"
    assert stream.is_alive == True
    assert stream == TwitchStream("123", "Foobar Qux!", "2020-01-01T00:00:00Z")
    assert stream != TwitchStream("121", "Foobar Qux!", "2020-01-01T00:00:00Z")
    assert stream == TwitchStream("123", "Foobar", "2020-01-01T00:00:01Z")
    assert stream == TwitchStream("123", "Foobar", "2222-01-01T00:00:00Z")
    assert str(stream) == '123: 2020-01-01T00:00:00Z: Foobar Qux! (live)'
    path = os.path.join(tempfile.mkdtemp(), 'testing.conf')
    assert stream.store(path)
    assert TwitchStream.restore(path) == stream


def test_twitch_stream_empty():
    stream = TwitchStream()
    assert stream.id == ""
    assert stream.started_at == "2000-01-01T00:00:00Z"
    assert stream.title == ""
    assert stream.is_alive == False
    path = os.path.join(tempfile.mkdtemp(), 'testing.conf')
    assert stream.store(path)
    assert TwitchStream.restore(path) == stream
    assert str(stream) == ': 2000-01-01T00:00:00Z:  (offline)'
    assert TwitchStream.restore("test2") == TwitchStream()



