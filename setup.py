import re
import os
from setuptools import setup, find_packages
from codecs import open


def get_requires(filename):
    requirements = []
    with open(filename, 'rt') as req_file:
        for line in req_file.read().splitlines():
            if not line.strip().startswith("#"):
                requirements.append(line)
    return requirements


def load_version():
    filename = os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), "sharkatzor", "__init__.py"))
    with open(filename, "rt") as version_file:
        file_init = version_file.read()
        version = re.search("__version__ = '([0-9a-z.-]+)'", file_init)
        if not version:
            raise Exception(f"Could not find version in {filename}")
        return version.group(1)


project_requirements = get_requires("sharkatzor/requirements.txt")
test_requirements = get_requires("sharkatzor/requirements_test.txt")

setup(
    name='sharkatzor-twitch-notify',
    version=load_version(),
    python_requires='>=3.7',
    description='A Discord bot that notifies when a Twitch channel is live.',
    url='https://gitlab.com/uilianries/sharkatzor-twitch-notify',
    author='Uilian Ries',
    author_email='uilianries@gmail.com',
    license='MIT',
    classifiers=[
        'Development Status :: 4 - Beta',
        'Intended Audience :: End Users/Desktop',
        'Topic :: Communications :: Chat',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 3',
        'Natural Language :: Portuguese',
    ],
    keywords=['discord', 'chat', 'twitch', 'bot', 'notification'],
    packages=find_packages(exclude=test_requirements),
    install_requires=project_requirements,
    extras_require={'dev': test_requirements, 'test': test_requirements},
    package_data={'sharkatzor': ['*.txt']},
    entry_points={'console_scripts': ['sharkatzor-twitch-nofity=sharkatzor.sharkatzor_twitch_notify:main']}
)
